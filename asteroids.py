#
# Ein simples Asteroids Spiel
# PyGame Frontend
#

import pygame
from pygame.locals import *
import asteroids_base as asteroids

# Displaybreite
width = 800
# Displayhöhe
height = 600
# Schwarzwert für den Afterglow Effekt, je höher, je weniger starker Effekt 
C_AFTERGLOW_DAMP = 13

# Spiel initianlisieren
asteroids = asteroids.Game(width, height)

# Spielfeld initialisieren
pygame.init()
clock = pygame.time.Clock()
screen = pygame.display.set_mode((width, height))
surf = pygame.Surface((width, height))
grey = pygame.Surface((width, height))
# Je dunkler dieses Surface, um so weniger stark ist der Afterglow Effekt
grey.fill((C_AFTERGLOW_DAMP, C_AFTERGLOW_DAMP, C_AFTERGLOW_DAMP))

running = True
while running:
    # Schwarzer Hintergrund
    surf.fill((0, 0, 0))
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            # Spiel Beenden
            if event.key == K_ESCAPE:
                running = False
            # Schiessen     
            if event.key == K_SPACE:
                asteroids.shoot()
        elif event.type == QUIT:
            running = False
    # Spielerbewegungen
    pressed = pygame.key.get_pressed()
    if pressed[pygame.K_UP]:
        asteroids.up()
    if pressed[pygame.K_DOWN]:
        asteroids.down()
    if pressed[pygame.K_LEFT]:
        asteroids.left()
    if pressed[pygame.K_RIGHT]:
        asteroids.right()

    asteroids.nextFrame()

    for x in asteroids.drawables():
        pygame.draw.polygon(surf, x.color, x.drawable(), 3)

    running = running and asteroids.running

    # Surface darstellen
    screen.blit(grey, (0, 0), None, BLEND_RGB_SUB)
    screen.blit(surf, (0, 0), None, BLEND_RGB_MAX)
    pygame.display.flip()
    # Wir wollen 60 FPS Konstant haben
    clock.tick(60)
pygame.quit()
