#
# Ein simples Asteroids Spiel
# Basisklassen
#

import random
from math import *

# Multiplikator für Schiffbeschleunigung
C_THRUST = 0.040
# Drehgeschwindigkeit des Schiffs
C_SHIP_TURN_SPEED = 0.055
# Schussgeschwindigkeit (Pixel pro Frame)
C_SHOOT_SPEED = 5
# Maximale Asteroidengeschwindigkeitn (Pixel pro Frame)
C_ASTEROID_SPEED_MAX = 2

# Der context enthält alle Attribute, welche für jedes Objekt jeder Klasse identisch sind
context = {"width" : 0, "height" : 0}

# Movables sind zeichenbar und beweglich
class Movable:
    points = []
    pos = [0, 0]
    v = [0, 0]
    color = (0, 0, 0)
    spin = 0
    orientation = 0
    ttl = 1
    def inertia(self):
        # Bewegung
        self.pos[0] = self.pos[0] + self.v[0]
        self.pos[1] = self.pos[1] + self.v[1]
        if self.pos[0] > context["width"]:
            self.pos[0] = 0
        if self.pos[0] < 0:
            self.pos[0] = context["width"]
        if self.pos[1] > context["height"]:
            self.pos[1] = 0
        if self.pos[1] < 0:
            self.pos[1] = context["height"]
        # Drehung
        tPoints = []
        for p in self.points:
            pStrich0 = p[0] * cos(self.spin) - p[1] * sin(self.spin)
            pStrich1 = p[1] * cos(self.spin) + p[0] * sin(self.spin)
            tPoints.append([pStrich0, pStrich1])
        self.points = tPoints
    def normVector(self):
        return [self.v[0] * self.speed(), self.v[1] * self.speed()]
    def speed(self):
        return sqrt(self.v[0] ** 2 + self.v[1] ** 2)
    def distance(self, movable):
        return sqrt((self.pos[0] - movable.pos[0]) ** 2 + (self.pos[1] - movable.pos[1]) ** 2)
    def drawable(self):
        tPoints = []
        for p in self.points:
            pStrich0 = p[0] * cos(self.orientation) - p[1] * sin(self.orientation)
            pStrich1 = p[1] * cos(self.orientation) + p[0] * sin(self.orientation)
            tPoints.append([self.pos[0] + pStrich0, self.pos[1] + pStrich1])
        return tPoints
    def intersects(this, movable):
        # Basiert auf http://www.ariel.com.au/a/python-point-int-poly.html
        for p in movable.drawable():
            x = p[0]
            y = p[1]
            n = len(this.drawable())
            inside = False
            p1x,p1y = this.drawable()[0]
            for i in range(n+1):
                p2x,p2y = this.drawable()[i % n]
                if y > min(p1y,p2y):
                    if y <= max(p1y,p2y):
                        if x <= max(p1x,p2x):
                            if p1y != p2y:
                                xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                            if p1x == p2x or x <= xinters:
                                inside = not inside
                p1x,p1y = p2x,p2y
            if inside:
                return True
        return False
    
# Der Spieler
class Ship(Movable):
    def __init__(self):
        self.points = [[10, 0], [-10,7], [-5,0], [-10, -7]]
        self.color = (255, 255, 255)
        self.pos = [context["width"] / 2,context["height"] / 2]
        self.v = [0, -0.1]        
    def thrust(self, x):
        if self.speed() < 10:
            # gedrehter Normalvctor
            t = [cos(self.orientation), sin(self.orientation)]
            self.v = [self.v[0] + t[0] * x, self.v[1] + t[1] * x]
    def shoot(self):
        b = Bullet();
        b.pos = [self.pos[0], self.pos[1]]
        b.v = [cos(self.orientation) * C_SHOOT_SPEED, sin(self.orientation) * C_SHOOT_SPEED]
        return b

# Ein Schuss
class Bullet(Movable):
    def __init__(self):
        self.ttl = context["height"] / C_SHOOT_SPEED
        self.points = [[1,-1], [1,1], [-1,1]]
        self.color = (255, 0, 0)
    def inertia(self):
        self.ttl -= 1
        Movable.inertia(self)

# Ein Asteroid
class Asteroid(Movable):
    def __init__(self,parts):
        # Der Asteroid kann so oft geteilt werden
        self.parts = parts
        self.v = [(random.random() - 0.5) * C_ASTEROID_SPEED_MAX, (random.random() - 0.5) * C_ASTEROID_SPEED_MAX]
        self.spin = random.random() / 100
        self.color = (random.randint(0, 200),random.randint(0, 200),random.randint(0, 200))
        self.pos = [random.randint(0, context["width"]),random.randint(0, context["height"])]
        sides = random.randint(4, 7)
        self.points = []
        r = random.randint(self.minMax()[0], self.minMax()[1])
        p = 0
        for i in range(0, sides):
            self.points.append([r * cos(p), r * sin(p)])
            r = random.randint(self.minMax()[0], self.minMax()[1])
            p = p + pi * 2 / sides
    # Formel zur Errechnung der Minimalen und maximalen Ausdehnung eines Asteroiden in Abhängigkeit seiner Teile
    def minMax(self):
        return [5 + self.parts * 5, 10 + self.parts * 10]
    def split(self):
        if self.parts > 0:
            a1 = type(self)(self.parts - 1)
            a2 = type(self)(self.parts - 1)
            a1.pos = [self.pos[0], self.pos[1]]
            a2.pos = [self.pos[0], self.pos[1]]
            return [a1, a2]
        else:
            return []

class Game():
    def __init__(self, width, height):
        #Spielfeldgrösse
        context["width"] = width
        context["height"] = height
        # Spielobjekte initialisieren
        self.ship = Ship()
        self.roids = []
        self.shots = []
        # Anzahl Asteroiden
        self.numRoids = 3
        # Status des Spiels
        self.running = True
        # Maximale gleichzeitige Schüsse
        self.shotsLeft = 3
        
    def up(self):
        self.ship.thrust(C_THRUST)

    def down(self):
        self.ship.thrust(-C_THRUST)

    def left(self):
        self.ship.orientation += -C_SHIP_TURN_SPEED

    def right(self):
        self.ship.orientation += C_SHIP_TURN_SPEED

    def shoot(self):
        if self.shotsLeft > 0:
            self.shots.append(self.ship.shoot())
            self.shotsLeft -= 1

    def nextFrame(self):
        # Alle Spielobjekte bewegen
        for x in self.roids + self.shots + [self.ship]:
            x.inertia()
            if x.ttl == 0:
                self.shots.remove(x)
                self.shotsLeft += 1

        # Kollision Asteroid/ Spieler
        for a in self.roids:
            if a.intersects(self.ship):
                self.running = False

        # Kollision Asteroid/ Schuss
        for a in self.roids:
            for s in self.shots:
                if a.intersects(s):
                    self.shots.remove(s)
                    self.shotsLeft += 1
                    self.roids.remove(a)
                    self.roids.extend(a.split())

        if len(self.roids) == 0:
            for i in range (0, self.numRoids):
                self.roids.append(Asteroid(self.numRoids))
            self.numRoids += 1

    def drawables(self):
        return self.roids + self.shots + [self.ship]
