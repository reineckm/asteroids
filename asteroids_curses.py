#
# Ein simples Asteroids Spiel
# nCurses simple Frontend
#

import asteroids_base as asteroids
import curses
import time

# Spielfeld initialisieren
screen = curses.initscr()
dim = screen.getmaxyx()
curses.noecho()
curses.cbreak()
curses.curs_set(0)
screen.clear()
screen.keypad(True)
screen.nodelay(True)

# Spiel initianlisieren
asteroids = asteroids.Game(dim[1] * 10, dim[0] * 10)
running = True

def flat(arr):
    f = []
    for i in arr:
        for j in i.drawable():
            x = int(j[0] / 10)
            y = int(j[1] / 10)
            if x == 0:
                x = 1
            if y == 0:
                y = 1
            if x > dim[1]:
                x = dim[1]
            if y > dim[0]:
                y = dim[0]
            f.append([y, x])
    return f
            
while running:
    key = screen.getch()
    if key == ord('q'):
        running = False
    if key == ord(' '):
        asteroids.shoot()
    if key == curses.KEY_UP:
        asteroids.up()
    if key == curses.KEY_DOWN:
        asteroids.down()
    if key == curses.KEY_LEFT:
        asteroids.left()
    if key == curses.KEY_RIGHT:
        asteroids.right()

    old = flat(asteroids.drawables())
    asteroids.nextFrame()
    new = flat(asteroids.drawables())
    for i in new:
        try:
            screen.addch(i[0], i[1], "█")
        except (curses.error):
            pass
        try:
            old.remove(i)
        except ValueError:
            pass        
    for i in old:
        try:
            screen.addch(i[0], i[1], " ")
        except (curses.error):
            pass
    
    time.sleep(1 / 60)
    running = running and asteroids.running
