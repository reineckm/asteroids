#
# Ein simples Asteroids Spiel
# nCurses advanced Frontend
#

import asteroids_base as asteroids
from skimage.draw import line
import numpy as np
import curses
import time

# Spielfeld initialisieren
screen = curses.initscr()
curses.def_shell_mode()
dim = screen.getmaxyx()
curses.noecho()
curses.cbreak()
curses.curs_set(0)
screen.clear()
screen.keypad(True)
screen.nodelay(True)

# Spiel initianlisieren
asteroids = asteroids.Game((dim[1] - 4) * 8, (dim[0] - 4) * 8)
running = True

def clip(i, min, max):
    if i < min:
        return min
    if i > max:
        return max
    return i

def paint(arr):
    w = dim[1] * 2
    h = dim[0] * 2
    field = np.zeros((w, h))
    for i in arr:
        x1 = None
        for j in i.drawable():            
            if not x1:
                x1 = clip(int(j[0] / 4), 0, w - 2)
                y1 = clip(int(j[1] / 4), 0, h - 2)
                x0 = x1
                y0 = y1
                continue
            x2 = clip(int(j[0] / 4), 0, w - 2)
            y2 = clip(int(j[1] / 4), 0, h - 2)
            rr, cc = line(x1, y1, x2, y2)
            field[rr, cc] = 1
            x1 = x2
            y1 = y2
        rr, cc = line(x0, y0, x2, y2)
        field[rr, cc] = 1
    return field

def code(c):
    if c == 0:
        return " "
    if c == 1:
        return "▘"
    if c == 2:
        return "▝"
    if c == 3:
        return "▀"
    if c == 4:
        return "▖"
    if c == 5:
        return "▌"
    if c == 6:
        return "▞"
    if c == 7:
        return "▛"
    if c == 8:
        return "▗"
    if c == 9:
        return "▚"
    if c == 10:
        return "▐"
    if c == 11:
        return "▜"
    if c == 12:
        return "▄"
    if c == 13:
        return "▙"
    if c == 14:
        return "▟"
    if c == 15:
        return "█"

field = paint(asteroids.drawables())
while running:
    key = screen.getch()
    if key == ord('q'):
        running = False
    if key == ord(' '):
        asteroids.shoot()
    if key == curses.KEY_UP:
        asteroids.up()
    if key == curses.KEY_DOWN:
        asteroids.down()
    if key == curses.KEY_LEFT:
        asteroids.left()
    if key == curses.KEY_RIGHT:
        asteroids.right()
    x = 0
    for i in range(0, int(len(field) / 2)):
        y = 0        
        for j in range(0, int(len(field[i]) / 2)):
            try:
                if field[x][y] == 1:
                    screen.addch(int(y / 2), int(x / 2), " ")
                    continue
                if field[x + 1][y] == 1:
                    screen.addch(int(y / 2), int(x / 2), " ")
                    continue
                if field[x][y + 1] == 1:
                    screen.addch(int(y / 2), int(x / 2), " ")
                    continue
                if field[x + 1][y + 1] == 1:
                    screen.addch(int(y / 2), int(x / 2), " ")
                    continue
            except (curses.error):
                pass
            y += 2
        x += 2    
    asteroids.nextFrame()
    field = paint(asteroids.drawables())    
    x = 0
    for i in range(0, int(len(field) / 2)):
        y = 0        
        for j in range(0, int(len(field[i]) / 2)):
            c = 0
            if field[x][y] == 1:
                c += 1
            if field[x + 1][y] == 1:
                c += 2    
            if field[x][y + 1] == 1:
                c += 4
            if field[x + 1][y + 1] == 1:
                c += 8
            try:
                screen.addch(int(y / 2), int(x / 2), code(c))
            except (curses.error):
                pass
            y += 2
        x += 2
    running = running and asteroids.running
curses.echo()
curses.curs_set(1)
curses.endwin()
